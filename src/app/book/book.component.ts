import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { OptionsService } from '../services/options.service';
import { BooksService } from '../services/books.service';


@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['book.component.css']
})
export class BookComponent implements OnInit {
  form : FormGroup;
  formats : string[];
  countries : string[];
  cities : string[];
  companies : string[];

  bookData : any = {
    author : '',
    cityId : '',
    companyId : '',
    countryId : '',
    description : '',
    formatId : '',
    id : '',
    isbn : '',
    pages : '',
    price : '',
    title : ''
  };

  loader: boolean = true;

  book_url_id : any = 0;
  page_status: string = 'addNewBook';

  constructor(
    private OptionsService : OptionsService,
    private BooksService : BooksService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  formInit() {

    this.form = new FormGroup({
      author: new FormControl(this.bookData.author, [Validators.required]),
      title: new FormControl(this.bookData.title, [Validators.required]),
      isbn: new FormControl(this.bookData.isbn, Validators.required),
      pages: new FormControl(this.bookData.pages, Validators.required),
      format: new FormControl(this.bookData.formatId, Validators.required),
      description: new FormControl(this.bookData.description, Validators.required),
      price: new FormControl(this.bookData.price, Validators.required),
      country: new FormControl(this.bookData.countryId, Validators.required),
      city: new FormControl(this.bookData.cityId, Validators.required),
      company: new FormControl(this.bookData.companyId, Validators.required)
    });

  }

  ngOnInit() {

    if ( this.page_status != 'success_added') {

      this.route.params.subscribe(params => {

        if (params['id'] !== undefined) {
          this.book_url_id = +params['id']; // (+) converts string 'id' to a number
          this.page_status = 'updateBook';

          this.BooksService.getBook(this.book_url_id).subscribe( res => {

            this.bookData = res;

            this.formInit();

          });

          // getBook
        }
      });

      this.OptionsService.getAllFormats().subscribe((res) => {
        this.formats = res;
      }, (error) => {
        console.log(error);
      });

      this.OptionsService.getAllCountries().subscribe((res) => {
        this.countries = res;
      }, (error) => {
        console.log(error);
      });

      this.OptionsService.getAllCities().subscribe((res) => {
        this.cities = res;
      }, (error) => {
        console.log(error);
      });

      this.OptionsService.getAllCompanies().subscribe((res) => {
        this.companies = res;
        this.loader = false;

      }, (error) => {
        console.log(error);
      });

      this.formInit();

    }

  }

  onSubmit() {

    this.bookData = {
      author      : this.form.value.author,
      title       : this.form.value.title,
      isbn        : this.form.value.isbn,
      pages       : this.form.value.pages,
      countryId   : this.form.value.country,
      cityId      : this.form.value.city,
      companyId   : this.form.value.company,
      formatId    : this.form.value.format,
      description : this.form.value.description,
      price       : this.form.value.price
    };


    if (this.page_status == 'addNewBook') {

      this.BooksService.addNewBook(this.bookData).subscribe((res ) => {
        this.page_status = 'success_added';
      },( error ) => {
        console.log(error);
      });

    } else {

      this.BooksService.updateBook(this.book_url_id, this.bookData).subscribe((res ) => {
        this.router.navigate(['/showcase']);
      },( error ) => {
        console.log(error);
      });

    }



  } // end onSubmit

}
