import {Component, OnInit, OnChanges, Input, Output, EventEmitter} from '@angular/core';
import { BooksService } from '../services/books.service';

@Component({
  selector: 'app-showcase',
  templateUrl: './showcase.component.html'
})
export class ShowcaseComponent implements OnInit, OnChanges {

  @Input() filtredBooks = [];git
  @Input() componentMode = false;
  @Output() deletedBookID = new EventEmitter<number>();

  books_list = [];
  connectionError = false;

  constructor(private BooksService: BooksService) { }

  ngOnInit() {

    if (this.componentMode == false) {
      this.BooksService.getAllBooks().subscribe((res ) => {
        this.books_list = res;
      },( error ) => {
        this.connectionError = true;
        console.log(error);
      });
    }

  } // ngOnInit

  ngOnChanges() {
    this.books_list = this.filtredBooks;
  } // ngOnChanges

  deleteBook(id) {

    let access = prompt('please enter to confirm: yes');

    if (access === 'yes') {
      this.BooksService.deleteBook(id).subscribe( res => {

        this.books_list = this.books_list.filter( book => {
          return Number(book.id) !== Number(id);
        });

        this.deletedBookID.emit(id);

        alert('Book successfully deleted');
      });
    }
  } // deleteBook

}
