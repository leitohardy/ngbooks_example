import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { OptionsService } from '../services/options.service';
import { BooksService } from '../services/books.service';
import { ActivatedRoute, Router } from '@angular/router';



@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit {

  searchForm = {
    author : '',
    title : '',
    isbn : '',
    formatId : 0,
    pageMin : 0,
    pageMax : 0,
    priceMin : 0,
    priceMax : 0
  }

  books  = [];
  booksFound  = [];

  constructor(
    private OptionsService : OptionsService,
    private location: Location,
    private BooksService : BooksService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit() {

    this.BooksService.getAllBooks().subscribe( res => {
      this.books = res;
      this.booksFound = this.books;

      this.route.queryParams.subscribe(params => {
        if (params['formatId'] !== undefined) {
          this.searchForm.formatId = +params['formatId'];
        }

        if (params['author'] !== undefined) {
          this.searchForm.author = params['author'];
        }
        this.onChange();
      });

    });

  }

  encodeUrl(str) {
    return str.replace(new RegExp('&', 'g'), "___");
  }

  decodeUrl(str) {
    return str.replace(new RegExp('___', 'g'), "&");
  }

  onChange() { // filter logic

    this.location.go(
      '/search',
      `?formatId=${this.searchForm.formatId}&author=${this.encodeUrl(this.searchForm.author)}`
    );

    this.booksFound = this.books;

    if (this.searchForm.author !== '') {

      let author = this.decodeUrl(this.searchForm.author);

      this.booksFound = this.booksFound.filter( item => {
        return item.author.toLowerCase().indexOf(  author.toLowerCase() )   !== -1;
      });
    }

    if (this.searchForm.title !== '') {
      this.booksFound = this.booksFound.filter( item => {
        return item.title.toLowerCase().indexOf(  this.searchForm.title.toLowerCase() )   !== -1;
      });
    }

    if (this.searchForm.isbn !== '') {
      this.booksFound = this.booksFound.filter( item => {
        return item.isbn.toLowerCase().indexOf(  this.searchForm.isbn.toLowerCase() )   !== -1;
      });
    }

    if ( Number(this.searchForm.formatId) !== 0) {
      this.booksFound = this.booksFound.filter( item => {
        return item.formatId == this.searchForm.formatId;
      });
    }

    if ( Number(this.searchForm.pageMax) !== 0 ) {
      this.booksFound = this.booksFound.filter( item => {
        return ( Number(this.searchForm.pageMin) <= item.pages && item.pages <= Number(this.searchForm.pageMax));
      });
    }

    if ( Number(this.searchForm.priceMax) !== 0 ) {
      this.booksFound = this.booksFound.filter( item => {
        return ( Number(this.searchForm.priceMin) <= item.price && item.price <= Number(this.searchForm.priceMax));
      });
    }

  } // onChange by Inputs


  changeSourceBooks(id) { // after delete some book
    this.books = this.books.filter( book => {
      return Number(book.id) !== Number(id);
    });
  }

}
