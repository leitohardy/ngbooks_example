import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable()
export class BooksService {

  headers = {
    headers: new Headers({
      'Content-Type':  'application/json',
      'x-auth-token' : 'bad18eba1ff45jk7858b8ae88a77fa30'
    })
  };
  constructor(private http: Http){}

  getBook(id){
    let url = 'http://localhost:3004/books/' + id;
    return this.http.get(url, this.headers)
      .map( (res) =>  res.json());
  }

  getAllBooks() {
    const url = 'http://localhost:3004/books';
    return this.http.get(url, this.headers)
      .map( (res) =>  res.json());
  }

  addNewBook(data) {
    const url = 'http://localhost:3004/books';
    return this.http.post(url, data, this.headers)
      .map( (res) =>  res.json());
  }

  updateBook(bookID, data) {
    const url = 'http://localhost:3004/books/' + bookID;
    return this.http.put(url, data, this.headers)
      .map( (res) =>  res.json());
  }

  deleteBook(id) {
    const url = 'http://localhost:3004/books/' + id;
    return this.http.delete(url, this.headers).map( (res) =>  res.json());
  }


}
