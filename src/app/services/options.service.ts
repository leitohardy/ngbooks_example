import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable()
export class OptionsService {

  constructor(private http: Http){}

  headers = {
    headers: new Headers({
      'Content-Type':  'application/json',
      'x-auth-token' : 'bad18eba1ff45jk7858b8ae88a77fa30'
    })
  };

  getAllFormats() {

    let url = 'http://localhost:3004/formats';

    return this.http.get(url, this.headers)
      .map( (res) =>  res.json());
  }

  getAllCountries() {

    let url = 'http://localhost:3004/countries';

    return this.http.get(url, this.headers)
      .map( (res) =>  res.json());
  }

  getAllCities() {

    let url = 'http://localhost:3004/cities';

    return this.http.get(url, this.headers)
      .map( (res) =>  res.json());
  }

  getAllCompanies() {

    let url = 'http://localhost:3004/companies';

    return this.http.get(url, this.headers)
      .map( (res) =>  res.json());
  }



}
