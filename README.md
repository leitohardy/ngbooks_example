# Project setup
```
git clone git@bitbucket.org:leitohardy/ngbooks_example.git
npm install
node server.js
ng serve --open
```

# Endpoints
```
  http://localhost:3004/books
  http://localhost:3004/formats
  http://localhost:3004/countries
  http://localhost:3004/cities
  http://localhost:3004/companies
```

# Request Authentication header
```
For retrieving data every request should be supplied with "x-auth-token: bad18eba1ff45jk7858b8ae88a77fa30" header.
```
